package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;

public class Auth extends Command{

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("502 ok", writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

