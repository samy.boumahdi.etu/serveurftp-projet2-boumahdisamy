package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;

public class User extends Command{

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("331 Please specify the password", writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


/*
    public void user() {
        if (this.reader.equals("USER anonymous")) {
            try {
                sendMessage("331 Please specify the password");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                sendMessage("530 This FTP server is anonymous only");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

     */
