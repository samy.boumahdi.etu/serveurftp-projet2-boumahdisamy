package sr.serveurftp.commande;

import java.io.PrintWriter;

public abstract class Command {

    public abstract void command(PrintWriter writer);

}


