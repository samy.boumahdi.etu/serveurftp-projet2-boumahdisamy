package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;

public class Password extends Command{

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("230 Login Successful", writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
