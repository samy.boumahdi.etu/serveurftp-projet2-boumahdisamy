package sr.serveurftp.commande;

import sr.serveurftp.Data;
import sr.serveurftp.Helper;
import sr.serveurftp.RequestHandler;

import java.io.IOException;
import java.io.PrintWriter;

public class List extends Command{

    protected Data data;

    public List(RequestHandler req) throws IOException {
        data = new Data(req);
    }

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("150 list rep", writer);
            data.allFiles();
            Helper.sendMessage("226 ok", writer);
        } catch (IOException e) {
            writer.println("550 Fail");
        }
    }

}