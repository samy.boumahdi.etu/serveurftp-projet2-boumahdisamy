package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;

public class Syst extends Command{

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("215 : UNIX", writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
