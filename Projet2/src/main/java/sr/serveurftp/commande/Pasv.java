package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.Random;

public class Pasv extends Command {

    private ServerSocket serverSocket;
    private Random random;

    @Override
    public void command(PrintWriter writer) {
        random = new Random();
        int port1 = this.random.nextInt(256);
        int port2 = this.random.nextInt(256);
        try {
            serverSocket = new ServerSocket(port1*256 + port2);
            Helper.sendMessage("227 Entering Passive Mode (127,0,0,1," + port1 + "," + port2 + ").", writer);
            Thread thread = new Thread(String.valueOf(serverSocket));
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
