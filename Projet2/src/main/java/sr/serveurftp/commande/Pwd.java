package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;

public class Pwd extends Command{

    private String directory = "/";

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("257 \"" + directory + "\" is the current directory.", writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
