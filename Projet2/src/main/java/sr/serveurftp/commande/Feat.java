package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;

public class Feat extends Command{

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("211-Features:", writer);
            Helper.sendMessage("PASV", writer);
            Helper.sendMessage("211 End", writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
