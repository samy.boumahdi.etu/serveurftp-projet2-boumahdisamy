package sr.serveurftp.commande;

import sr.serveurftp.Helper;

import java.io.IOException;
import java.io.PrintWriter;

public class Type extends Command{

    @Override
    public void command(PrintWriter writer) {
        try {
            Helper.sendMessage("200 Switching to Binary mode", writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}