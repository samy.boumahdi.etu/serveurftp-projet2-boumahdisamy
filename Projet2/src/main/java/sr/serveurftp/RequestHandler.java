package sr.serveurftp;

import java.io.*;
import java.net.Socket;

import sr.serveurftp.commande.*;

public class RequestHandler extends Thread {

    private BufferedReader reader;
    private PrintWriter writer;
    public Socket socket;
    private String path = "/";

    public RequestHandler(Socket socket) {
        this.socket = socket;
    }

    public String getPath() {
        return this.path;
    }


    @Override
    public void run() {
        try {

            System.out.println("Nouvelle connection");

            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

            writer.println("220 FTP server");

            String line = reader.readLine();
            while (line != null && line.length() > 0) {
                if (line.startsWith("AUTH")) {
                    new Auth().command(writer);
                } else if (line.startsWith("USER")) {
                    new User().command(writer);
                } else if (line.startsWith("PASS")) {
                    new Password().command(writer);
                } else if (line.startsWith("PWD")){
                    new Pwd().command(writer);
                }else if (line.startsWith("FEAT")) {
                    new Feat().command(writer);
                }else if (line.startsWith("SYST")) {
                    new Syst().command(writer);
                }else if (line.startsWith("TYPE")) {
                    new Type().command(writer);
                }else if (line.startsWith("PASV")) {
                    new Pasv().command(writer);
                }else if (line.startsWith("LIST")) {
                    new List(this).command(writer);
                }

                line = reader.readLine();
            }

            socket.close();

            System.out.println("Connection fermée");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
