package sr.serveurftp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerFTP extends Thread {
    private ServerSocket serverSocket;
    private int port;
    private boolean running = false;

    public ServerFTP(int port) {
        this.port = port;
    }

    public void startServer() {
        try {
            serverSocket = new ServerSocket(port);
            this.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopServer() {
        running = false;
        this.interrupt();
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                System.out.println("En attente d'une connexion");

                Socket socket = serverSocket.accept();

                RequestHandler requestHandler = new RequestHandler(socket);
                requestHandler.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
